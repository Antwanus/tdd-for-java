package com.antoonvereecken.stockmanagement;

import com.antoonvereecken.dataservice.ExternalISBNDataService;
import com.antoonvereecken.domain.Book;

public class StockManager {
   private ExternalISBNDataService dbService;
   private ExternalISBNDataService webDataService;

   public void setDbService(ExternalISBNDataService dbService) { this.dbService = dbService; }
   public void setWebDataService(ExternalISBNDataService webDataService) { this.webDataService = webDataService; }

   public String getLocationIdByISBN(String isbn) {
      Book b = dbService.lookUp(isbn);
      if (b == null) {
         b = webDataService.lookUp(isbn);
      }
      StringBuilder sb = new StringBuilder();
      sb.append( isbn.substring(isbn.length()-4));
      sb.append( b.getAuthor().charAt(0));
      sb.append( b.getTitle().split(" ").length );

      return sb.toString();
   }
}
