package com.antoonvereecken.isbntools;

public class ISBNValidation {

   public static final int LONG_ISBN_LENGTH = 13;
   public static final int LONG_ISBN_MULTIPLIER = 10;
   public static final int SHORT_ISBN_LENGTH = 10;
   public static final int SHORT_ISBN_MULTIPLIER = 11;

   public boolean checkISBN(String isbn) {
      if (isbn.length() == LONG_ISBN_LENGTH) {
         return isThisAValid13DigitISBN(isbn);
      }
      else if (isbn.length() == SHORT_ISBN_LENGTH) {
         return isThisAValid10DigitISBN(isbn);
      }
      throw new NumberFormatException("ISBN numbers must be 10 digits long");

   }

   private boolean isThisAValid10DigitISBN(String isbn) {
      var total = 0;
      for (var i = 0; i < SHORT_ISBN_LENGTH; i++) {
         if (!Character.isDigit(isbn.charAt(i))) {
            if (i == 9 && isbn.charAt(i) == 'X')
               total += 10;
            else
               throw new NumberFormatException("ISBN numbers can only contain numeric digits");
         }
         else
            total += Character.getNumericValue(isbn.charAt(i)) * (SHORT_ISBN_LENGTH - i);
      }
      return total % SHORT_ISBN_MULTIPLIER == 0;
   }

   private boolean isThisAValid13DigitISBN(String isbn) {
      var total = 0;
      for (var i = 0; i < LONG_ISBN_LENGTH; i++) {
         if (i % 2 == 0) {
            total += Character.getNumericValue(isbn.charAt(i));
         }
         else {
            total += Character.getNumericValue(isbn.charAt(i)) * 3;
         }
      }
      return total % LONG_ISBN_MULTIPLIER == 0;
   }

   /** My version from challenge_3 */
   public boolean myCheckISBN(String isbn) {
      if (isbn.length() == 13) {
         var total = 0;
         int checkDigit = Character.getNumericValue(isbn.charAt(12));
         for (var i = 0; i < 12; i++) {
            if (i % 2 == 0)   total += Character.getNumericValue(isbn.charAt(i));
            else  total += Character.getNumericValue(isbn.charAt(i)) * 3;
         }
         return 10 - total % 10 == checkDigit;
      }
      else {
         if (isbn.length() != 10)
            throw new NumberFormatException("ISBN numbers must be 10 digits long");
         var total = 0;
         for (var i = 0; i < 10; i++) {
            if (!Character.isDigit(isbn.charAt(i))) { // if current index is not a digit
               // last index of ISBN && == 'X' ?   x=10
               if (i == 9 && isbn.charAt(i) == 'X')   total += 10;
               else   throw new NumberFormatException("ISBN numbers can only contain numeric digits");
            }
            else   total += Character.getNumericValue(isbn.charAt(i)) * (10 - i);
         }
         return total % 11 == 0;
      }
   }


}