package com.antoonvereecken.dataservice;

import com.antoonvereecken.domain.Book;

public interface ExternalISBNDataService {
   Book lookUp(String isbn);

}
