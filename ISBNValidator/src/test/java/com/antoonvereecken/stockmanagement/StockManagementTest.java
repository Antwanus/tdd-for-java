package com.antoonvereecken.stockmanagement;

import com.antoonvereecken.dataservice.ExternalISBNDataService;
import com.antoonvereecken.domain.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StockManagementTest {

   ExternalISBNDataService testDbService;
   ExternalISBNDataService testWebService;
   StockManager stockManager;

   @BeforeEach
   void setUp() {
      testDbService = mock(ExternalISBNDataService.class);
      testWebService = mock(ExternalISBNDataService.class);

      stockManager = new StockManager();
      stockManager.setWebDataService(testWebService);
      stockManager.setDbService(testDbService);
   }

   @Test
   void generateValidWarehouseLocationId() {
      ExternalISBNDataService testWebService = new ExternalISBNDataService() {
         @Override
         public Book lookUp(String isbn) {
            return new Book(isbn, "Of Mice And Men", "J. Steinbeck");
         }
      };
      ExternalISBNDataService dbService = new ExternalISBNDataService() {
         @Override
         public Book lookUp(String isbn) {
            return null;
         }
      };

      stockManager = new StockManager();
      stockManager.setWebDataService(testWebService);
      stockManager.setDbService(dbService);

      String isbn = "0140177396";
      String locatorCode = stockManager.getLocationIdByISBN(isbn);
      assertEquals("7396J4", locatorCode);
   }
   @Test
   void generateValidWarehouseLocatorCodeWithMockito() {
      when(testDbService.lookUp(anyString())).thenReturn(null);
      when(testWebService.lookUp("0140177396")).thenReturn(
                     new Book("0140177396", "Of Mince And Men", "J. Steinbeck")
      );

      String locatorCode = stockManager.getLocationIdByISBN("0140177396");
      assertEquals("7396J4", locatorCode);
   }

   @Test
   void databaseServiceIsUsedIfDataIsPresentInDB() {
      //record found in DB
      when(testDbService.lookUp("0140177396")).thenReturn(new Book("0140177396", "testTitle", "testAuthor"));

      StockManager stockManager = new StockManager();
      stockManager.setDbService(testDbService);
      stockManager.setWebDataService(testWebService);

      stockManager.getLocationIdByISBN("0140177396");

      verify(testDbService, times(1)).lookUp("0140177396");
      verify(testWebService, never()).lookUp( anyString() );
   }

   @Test
   void webDataServiceIsUsedIfDataIsNotPresentInDB() {
      ExternalISBNDataService dbService = mock(ExternalISBNDataService.class);
      ExternalISBNDataService webDataService = mock(ExternalISBNDataService.class);

      when(dbService.lookUp("0140177396")).thenReturn(null);   // no record in DB for isbn
      when(webDataService.lookUp("0140177396"))
          .thenReturn(new Book("0140177396", "testTitle", "testAuthor"));

      StockManager stockManager = new StockManager();
      stockManager.setDbService(dbService);
      stockManager.setWebDataService(webDataService);

      stockManager.getLocationIdByISBN("0140177396");

      verify(dbService).lookUp("0140177396");
      verify(webDataService).lookUp("0140177396");
   }
}
