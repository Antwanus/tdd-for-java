package com.antoonvereecken.isbntools;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ISBNValidationTest {

   @Test
   void validISBN13ReturnsTrue() {
      ISBNValidation validator = new ISBNValidation();
      boolean result = validator.checkISBN("9781853260087");
      assertTrue(result, "valid ISBN: 9780306406157");
      result = validator.checkISBN("9781853267338");
      assertTrue(result, "valid ISBN: 9781853267338");
   }
   @Test
   void validISBN10ReturnsTrue() {
      ISBNValidation validator = new ISBNValidation();
      boolean result = validator.checkISBN("0140449116");
      assertTrue(result, "ISBN 0140449116 is valid");
      result = validator.checkISBN("0140177396");
      assertTrue(result, "ISBN 0140177396 is valid");
   }
   @Test
   void validISBN10EndingInAnXReturnsTrue() {
      ISBNValidation validator = new ISBNValidation();
      boolean result = validator.checkISBN("012000030X");
      assertTrue(result);
   }

   @Test
   void invalidISBN10ReturnsFalse() {
      ISBNValidation validator = new ISBNValidation();
      boolean result = validator.checkISBN("0140449117");
      assertFalse(result, "ISBN 0140449117 is NOT valid");
   }
   @Test
   void invalidISBN13ReturnsFalse() {
      ISBNValidation validator = new ISBNValidation();
      assertFalse(
          validator.checkISBN("9780306406158"),
          "ISBN 9780306406158 is NOT valid"
      );
   }
   @Test
   void invalidISBNWith9DigitsThrowsNumberFormatException() {
      ISBNValidation validator = new ISBNValidation();
      assertThrows(
          NumberFormatException.class,
          ()-> validator.checkISBN("123456789"),
          "ISBN 123456789 is invalid (length != 10)"
      );
   }
   @Test
   void invalidISBNWithStringFormatThrowsNumberFormatException() {
      ISBNValidation validator = new ISBNValidation();
      assertThrows(
          NumberFormatException.class,
          ()-> validator.checkISBN("helloworld"),
          "ISBN helloworld is invalid (not a number)"
      );
   }

   /*       ---  myCheckISBN ---       */
   @Test
   void myValidISBN13ReturnsTrue() {
      ISBNValidation validator = new ISBNValidation();
      assertTrue( validator.myCheckISBN("9781853260087"), "valid ISBN: 9781853260087");
      assertTrue( validator.myCheckISBN("9781853267338"), "valid ISBN: 9781853267338");
   }
   @Test
   void myValidISBN10ReturnsTrue() {
      ISBNValidation validator = new ISBNValidation();
      assertTrue( validator.myCheckISBN("0140449116"), "ISBN 0140449116 is valid");
      assertTrue( validator.myCheckISBN("0140177396"), "ISBN 0140177396 is valid");
   }
   @Test
   void myValidISBN10EndingInAnXReturnsTrue() {
      ISBNValidation validator = new ISBNValidation();
      assertTrue( validator.myCheckISBN("012000030X"));
   }
   @Test
   void myInvalidISBN10ReturnsFalse() {
      ISBNValidation validator = new ISBNValidation();
      assertFalse( validator.myCheckISBN("0140449117"), "ISBN 0140449117 is NOT valid");
   }

   @Test
   void myInvalidISBN13ReturnsFalse() {
      ISBNValidation validator = new ISBNValidation();
      assertFalse( validator.myCheckISBN("9780306406158"), "ISBN 9780306406158 is NOT valid");
   }

   @Test
   void myInvalidISBNWith9DigitsThrowsNumberFormatException() {
      ISBNValidation validator = new ISBNValidation();
      assertThrows(     NumberFormatException.class,
                        ()-> validator.myCheckISBN("123456789"),
               "ISBN 123456789 is invalid (length != 10)"
      );
   }
   @Test
   void myInvalidISBNWithStringFormatThrowsNumberFormatException() {
      ISBNValidation validator = new ISBNValidation();
      assertThrows(     NumberFormatException.class,
                        ()-> validator.myCheckISBN("helloworld"),
               "ISBN helloworld is invalid (not a number)"
      );
   }
}
