package com.antoonvereecken.loan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class LoanCalcControllerNewLoanApplicationTest {

   @Spy LoanApplication loanApp;
   LoanCalculatorController controller;

   @BeforeEach
   public void setup() {
      loanApp = spy(new LoanApplication());
      controller = new LoanCalculatorController();

      LoanRepository repository = mock(LoanRepository.class);
      JavaMailSender mailSender = mock(JavaMailSender.class);
      RestTemplate restTemplate = mock(RestTemplate.class);

      controller.setData(repository);
      controller.setMailSender(mailSender);
      controller.setRestTemplate(restTemplate);
   }

   @Test
   void test1YearLoan(){
      loanApp.setPrincipal(1200);
      loanApp.setTermInMonths(12);
      doReturn( new BigDecimal(10)).when(loanApp).getInterestRate();

      controller.processNewLoanApplication(loanApp);

      assertEquals( new BigDecimal(110), loanApp.getRepayment());
   }
   @Test
   void test2YearLoan(){
      loanApp.setPrincipal(1200);
      loanApp.setTermInMonths(24);
      doReturn( new BigDecimal(10)).when(loanApp).getInterestRate();
      controller.processNewLoanApplication(loanApp);

      assertEquals( new BigDecimal(60), loanApp.getRepayment());
   }
   @Test
   void test5YearLoanWithRounding(){
      loanApp.setPrincipal(5000);
      loanApp.setTermInMonths(60);
      doReturn( new BigDecimal("6.5")).when(loanApp).getInterestRate();
      controller.processNewLoanApplication(loanApp);

      assertEquals( new BigDecimal(111), loanApp.getRepayment());
   }


}
