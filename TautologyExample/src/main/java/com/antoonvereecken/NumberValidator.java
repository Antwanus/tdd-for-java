package com.antoonvereecken;

public class NumberValidator {

   public boolean isItPrime(int number) {
      int maxDivisor = (int) Math.sqrt(number);
      for(var i=2; i<=maxDivisor; i++) {
         if(number % i == 0)
            return false;
      }
      return true;
   }


}
