package com.antoonvereecken;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberValidatorTests {

   @Test
   @Disabled("This test method contains business logic -> refactored into " +
       "[isItPrimeTestWithValidPrimeReturnsTrue, isItPrimeTestWithInvalidPrimeReturnsFalse ]")
   void isItPrimeTest() {
      Integer numbers[] = { 1,15,23,25,60,61,63,79,207 };
      NumberValidator validator = new NumberValidator();

      for (int i = 0; i < numbers.length; i++) {
         boolean isPrime = true;
         int maxDivisor = (int) Math.sqrt(numbers[i]);

         for(int counter =2; counter < maxDivisor; counter++) {
            if(numbers[i] % counter ==0)
               isPrime = false;
         }

         assertEquals(isPrime, validator.isItPrime(numbers[i]));
      }
   }

   @Test
   void isItPrimeTestWithValidPrimeReturnsTrue() {
      Integer[] numbers = { 1, 3, 5, 7, 11, 23, 61, 79 };
      NumberValidator validator = new NumberValidator();
      for (Integer number : numbers) {
         assertTrue(validator.isItPrime(number));
      }
   }

   @Test
   void isItPrimeTestWithInvalidPrimeReturnsFalse() {
      Integer[] numbers = { 4, 6, 8, 10 };
      NumberValidator validator = new NumberValidator();
      for (Integer number : numbers) {
         assertFalse(validator.isItPrime(number), "number checked was " +number+ " -> this is not a prime");
      }
   }


}